$(document).ready(function(){

    var countryId = $('#select-country option:selected').val();

    if(countryId > 0){
        $('#loading-cities').show();
        $('#search-city-box').show();
        $('#select-city').html('');
        var request = $.ajax({
            url: "/api/country/"+countryId+"/cities",
            method: "GET",
            dataType: "json"
        });
        request.done(function( data ) {
            if(data.length > 0){
                var html = '';
                $.each(data, function( index, city ) {
                    // SELECTED_CITIES coming from front/sidebar.html.twig line# 30
                    var checked = ($.inArray( city.id, SELECTED_CITIES) != -1)? 'checked' : '';
                    html += '' +
                        '<li>' +
                        '<div class="custom-control custom-checkbox">' +
                        '<label class="form-check-label">' +
                        '<input ' + checked + ' name="cities[]" type="checkbox" class="custom-control-input" value="' + city.id + '">' +
                        '<span class="custom-control-indicator"></span>' +
                        '<span class="custom-control-description">' + city.name + '</span>' +
                        '</label>' +
                        '</div>' +
                        '</li>';
                });
                $('#select-city').html(html);
                $('#loading-cities').hide();
            }
            else{
                $('#search-city-box').hide();
            }
        });

        request.fail(function( jqXHR, textStatus ) {
            alert( "Request failed to Load Cities: " + textStatus );
        });
    }

    $('#select-country').change(function () {
        var countryId = $('#select-country option:selected').val();
        if(countryId > 0){
            $('#loading-cities').show();
            $('#search-city-box').show();
            $('#select-city').html('');
            var request = $.ajax({
                url: "/api/country/"+countryId+"/cities",
                method: "GET",
                dataType: "json"
            });

            request.done(function( data ) {
                if(data.length > 0){
                    var html = '';
                    $.each(data, function( index, city ) {
                        html += '' +
                            '<li>' +
                            '<div class="custom-control custom-checkbox">' +
                            '<label class="form-check-label">' +
                            '<input name="cities[]" type="checkbox" class="custom-control-input" value="' + city.id + '">' +
                            '<span class="custom-control-indicator"></span>' +
                            '<span class="custom-control-description">' + city.name + '</span>' +
                            '</label>' +
                            '</div>' +
                            '</li>';
                    });
                    $('#select-city').html(html);
                    $('#loading-cities').hide();
                }
                else{
                    $('#search-city-box').hide();
                }
            });

            request.fail(function( jqXHR, textStatus ) {
                alert( "Request failed to Load Cities: " + textStatus );
            });
        }
        else{
            $('#select-city').html('');
            $('#search-city-box').hide();
        }
    });

    $('#search-months-btn').click(function(){
        $('#search-months').slideDown('slow');
    });

    $('#search-block-btn').click(function(){
        $('#search-block').slideDown('slow');
    });
});
