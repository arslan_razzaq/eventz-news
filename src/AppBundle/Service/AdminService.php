<?php
namespace AppBundle\Service;

use AppBundle\Entity\Category;
use AppBundle\Entity\Page;
use Doctrine\ORM\EntityManagerInterface;

class AdminService
{

    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function addCategory($title, $orderBy)
    {
        try{
            $category = new Category();
            $category->setTitle($title);
            $category->setOrderBy($orderBy);
            $this->em->persist($category);
            $this->em->flush();
        }
        catch (Exception $e) {
            throw $e;
        }
    }

    public function updateCategory($category, $title, $orderBy)
    {
        try{
            $category->setTitle($title);
            $category->setOrderBy($orderBy);
            $this->em->flush();
        }
        catch (Exception $e) {
            throw $e;
        }
    }

    public function savePage(Page $page, $formData)
    {
        try{
            $page->setTitle($formData->getTitle());
            $page->setContent($formData->getContent());
            $page->setMetaDescription($formData->getMetaDescription());
            $page->setMetaKey($formData->getMetaKey());
            $this->em->persist($page);
            $this->em->flush();
        }
        catch (Exception $e) {
            throw $e;
        }
    }

}