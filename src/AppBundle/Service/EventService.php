<?php

namespace AppBundle\Service;
use AppBundle\Entity\City;
use AppBundle\Entity\Event;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class EventService
{

    private $em;
    private $user;

    public function __construct(EntityManagerInterface $entityManager, TokenStorageInterface $tokenStorage)
    {
        $this->user = $tokenStorage->getToken()->getUser();
        $this->em = $entityManager;
    }

    private function uploadPoster($formData, $poster)
    {
        if($formData->getPoster() != null) {
            $file = $formData->getPoster();
            $fileName = md5(uniqid()) . '.' . $file->guessExtension();
            $file->move(
                $poster['uploadPath'],
                $fileName
            );
            return $fileName;
        }
        else if(isset($poster['fileName'])){
            return $poster['fileName'];
        }
        return null;
    }

    public function saveEvent(Event $conference, $formData, $poster, $formType)
    {
        try{
            $conference->setCategory($formData->getCategory());
            $conference->setTitle($formData->getTitle());
            $conference->setDescription($formData->getDescription());
            $conference->setStartDate($formData->getStartDate());
            $conference->setEndDate($formData->getEndDate());
            $conference->setVenue($formData->getVenue());
            $conference->setCountry($formData->getCountry());
            $conference->setCity($formData->getCity());
            $conference->setWebsite($formData->getWebsite());
            $conference->setCoupon($formData->getCoupon());
            $conference->setRegistrationLink($formData->getRegistrationLink());
            $conference->setPrice($formData->getPrice());
            $conference->setDiscount($formData->getDiscount());
            $conference->setDiscountType($formData->getDiscountType());
            $conference->setPoster($this->uploadPoster($formData, $poster));
            $conference->setPosterTitle($formData->getPosterTitle());
            $conference->setRibbonTitle($formData->getRibbonTitle());
            $conference->setRibbonColor($formData->getRibbonColor());
            $conference->setTitleText($formData->getTitleText());
            $conference->setMetaDescription($formData->getMetaDescription());
            $conference->setMetaKey($formData->getMetaKey());
            $conference->setDisplayOrder($formData->getDisplayOrder());
            $conference->setStatus($this->setEventStatus($formData));
            if($formType == 'add'){
                $conference->setUser($this->user);
                $this->em->persist($conference);
            }
            $this->em->flush();
        }
        catch (Exception $e) {
            throw $e;
        }
    }

    public function deleteEvent(Event $conference)
    {
        try{
            $conference->setStatus(2);
            $this->em->flush();
            return TRUE;
        }
        catch (Exception $e) {
            throw $e;
        }
    }

    private function setEventStatus($formData)
    {
        if($formData->getStatus() == null){
            return in_array('ROLE_ADMIN', $this->user->getRoles())? 1 : 0;
        }
        return $formData->getStatus();
    }

}