<?php
namespace AppBundle\Service;

use AppBundle\Entity\Organizer;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Swift_Image;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class OrganizerService
{

    private $em;
    private $mailer;
    private $templating;

    public function __construct(EntityManagerInterface $entityManager, \Swift_Mailer $mailer, ContainerInterface $container)
    {
        $this->em = $entityManager;
        $this->mailer = $mailer;
        $this->container = $container;
        $this->templating = $container->get('templating');
    }

    public function organizerRegister($formData)
    {
        $this->em->getConnection()->beginTransaction();
        try {
            $user = new User;
            $user->setUsername($formData->getUsername());
            $user->setRoles(['ROLE_ORGANIZER']);
            $user->setPlainPassword($formData->getPassword());
            $this->em->persist($user);

            $organizer = new Organizer;
            $organizer->setUser($user);
            $organizer->setName($formData->getName());
            $organizer->setEmail($formData->getEmail());
            $organizer->setMobile($formData->getMobile());
            $this->em->persist($organizer);

            $this->em->flush();
            $this->em->getConnection()->commit();
        } catch (Exception $e) {
            $this->em->getConnection()->rollBack();
            //throw $e;
            return FALSE;
        }
        return $user;
    }

    public function sendThankyouEmail($username, $organizerName, $organizerEmail)
    {
        $mailerImageUrl = $this->container->getParameter('mailer_image_url');
        $message = \Swift_Message::newInstance();
        $headerLogo = $message->embed(Swift_Image::fromPath($mailerImageUrl.'/assets/images/logo-white-email.png'));
        /* // Translate and pass variable
        $message->setSubject($this->translator->trans("Invitación a la comunidad %community%", array(
            '%community%' => $community->getNiceName()
        )))
        */
        $message->setSubject('Thank You For Registration in Events Discount.')
            ->setFrom('no-reply@eventsdiscount.com')
            ->setTo($organizerEmail)
            ->setBody(
                $this->templating->render('emails/register-thankyou.html.twig',[
                    'headerLogo'    => $headerLogo,
                    'organizerName' => $organizerName,
                    'username' => $username
                ]),
                'text/html'
            )
        ;
        return $this->mailer->send($message);
    }

}