<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Gregwar\CaptchaBundle\Type\CaptchaType;

class PromoteMyEventFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('yourName', TextType::class, [
                'label'       => 'Your Name',
                'constraints' => [new NotBlank()]
            ])
            ->add('yourEmail', EmailType::class, [
                'label'       => 'Your Email',
                'constraints' => [new NotBlank()]
            ])
            ->add('yourMobile', TextType::class, [
                'label'       => 'Your Mobile No',
                'constraints' => [new NotBlank()]
            ])
            ->add('captcha', CaptchaType::class)
        ;
    }
}
