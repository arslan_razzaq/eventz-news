<?php

namespace AppBundle\Form\Admin;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category', EntityType::class, [
                'class' => 'AppBundle\Entity\Category',
                'placeholder' => '-- Select Category --'
            ])
            ->add('title')
            ->add('description')
            ->add('startDate', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['class' => 'startDate']
            ])
            ->add('endDate', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['class' => 'endDate']
            ])
            ->add('venue')
            ->add('country', EntityType::class, [
                'class' => 'AppBundle\Entity\Country',
                'placeholder' => '-- Select Country --'
            ])
            ->add('city', ChoiceType::class, [
                'placeholder' => '-- Select City --'
            ])
            ->add('website')
            ->add('coupon')
            ->add('registrationLink')
            ->add('displayOrder')
            ->add('ribbonTitle')
            ->add('ribbonColor')
            ->add('titleText')
            ->add('metaKey')
            ->add('metaDescription')
            ->add('price', TextType::class, ['label' => 'Actual Price'])
            ->add('discountType', ChoiceType::class, [
                'choices'  => [
                    'None' => null,
                    'Percentage (%)' => 'p',
                    'Price (SR)' => 'a',
                ]
            ])
            ->add('discount')
            ->add('status', ChoiceType::class, [
                'choices'  => [
                    'Active' => 1,
                    'Pending' => 0,
                    'Block' => 2,
                    'Closed' => 3,
                ]
            ])
            ->add('posterTitle')
            ->add('poster', FileType::class,
                [
                    'label' => 'Poster', 'data_class' => null,
                    'attr' => ['class' => 'form-control']
                ]
            );


        $setCityFunction = function(FormInterface $form, $country) {
            $form->add('city', EntityType::class, [
                'class' => 'AppBundle\Entity\City',
                'query_builder' => function (EntityRepository $er) use ($country) {
                    return $er->createQueryBuilder('c')
                        ->where('c.country = :country')
                        ->setParameter('country' , $country->getId());
                },
            ]);
        };

        $builder->get('country')->addEventListener(FormEvents::POST_SUBMIT, function(FormEvent $event) use ($setCityFunction) {
            $setCityFunction($event->getForm()->getParent(), $event->getForm()->getData());
        });
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Event'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_event';
    }


}
