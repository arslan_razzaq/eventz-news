<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Gregwar\CaptchaBundle\Type\CaptchaType;

class ContactFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('yourName', TextType::class, [
                'label'       => 'Your Name',
                'constraints' => [new NotBlank()]
            ])
            ->add('yourEmail', EmailType::class, [
                'label'       => 'Your Email',
                'constraints' => [new NotBlank()]
            ])
            ->add('yourSubject', TextType::class, [
                'label'       => 'Your Subject',
                'constraints' => [new NotBlank()]
            ])
            ->add('yourMessage', TextareaType::class, [
                'label'       => 'Your Message',
                'constraints' => [new NotBlank()]
            ])
            ->add('captcha', CaptchaType::class)
        ;
    }
}
