<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Assert\NotBlank(message="Please Insert Category Title.")
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Event", mappedBy="category")
     */
    private $events;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $orderBy;

    public function getId()
    {
        return $this->id;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getEvents()
    {
        return $this->events;
    }

    public function getEventsFilter()
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->gt('startDate', new \DateTime()))
            ->andWhere(Criteria::expr()->eq('status', 1))
            ->orderBy([
                'displayOrder' => Criteria::DESC,
                'startDate' => Criteria::ASC
            ])
            ->setFirstResult(0)
            ->setMaxResults(8);
        return $this->getEvents()->matching($criteria);
    }

    public function getOrderBy()
    {
        return $this->orderBy;
    }

    public function setOrderBy($orderBy)
    {
        $this->orderBy = $orderBy;
    }

    public function __toString()
    {
        return $this->getTitle();
    }

}

