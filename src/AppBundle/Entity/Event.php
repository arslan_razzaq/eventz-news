<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Event
 *
 * @ORM\Table(name="event")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EventRepository")
 */
class Event
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(onDelete="CASCADE", nullable = false)
     */
    private $user;

    /**
     * @Assert\NotBlank(message="Select Category.")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Category", inversedBy="events")
     * @ORM\JoinColumn(onDelete="CASCADE", nullable = false)
     */
    private $category;

    /**
     * @Assert\NotBlank(message="Insert Event Title.")
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;

    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @Assert\NotBlank(message="Insert Start Date.")
     * @ORM\Column(name="start_date", type="date")
     */
    private $startDate;

    /**
     * @Assert\NotBlank(message="Insert End Date.")
     * @ORM\Column(name="end_date", type="date")
     */
    private $endDate;

    /**
     * @Assert\NotBlank(message="Select Country.")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Country")
     * @ORM\JoinColumn(onDelete="CASCADE", nullable = false)
     */
    private $country;

    /**
     * @Assert\NotBlank(message="Select City.")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\City")
     * @ORM\JoinColumn(onDelete="CASCADE", nullable = false)
     */
    private $city;

    /**
     * @ORM\Column(name="venue", type="text", nullable=true)
     */
    private $venue;

    /**
     * @ORM\Column(name="website", type="string", length=255, nullable=true)
     */
    private $website;

    /**
     * @ORM\Column(name="poster", type="string", nullable=true)
     */
    private $poster;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $posterTitle;

    /**
     * @ORM\Column(name="coupon", type="string", length=255, nullable=true)
     */
    private $coupon;

    /**
     * @ORM\Column(name="registration_link", type="string", length=255, nullable=true)
     */
    private $registrationLink;

    /**
     * @Assert\NotBlank(message="Insert Price.")
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(name="discount", type="string", length=255, nullable=true)
     */
    private $discount;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $discountType;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $titleText;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $metaKey;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $metaDescription;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $displayOrder;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $ribbonTitle;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $ribbonColor;

    /**
     * @ORM\Column(type="integer")
     */
    private $status;

    public function getId()
    {
        return $this->id;
    }

    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
        return $this;
    }

    public function getStartDate()
    {
        return $this->startDate;
    }

    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
        return $this;
    }

    public function getEndDate()
    {
        return $this->endDate;
    }

    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setVenue($venue)
    {
        $this->venue = $venue;
        return $this;
    }

    public function getVenue()
    {
        return $this->venue;
    }

    public function setCmeHours($cmeHours)
    {
        $this->cmeHours = $cmeHours;
        return $this;
    }

    public function getCmeHours()
    {
        return $this->cmeHours;
    }

    public function setWebsite($website)
    {
        $this->website = $website;
        return $this;
    }

    public function getWebsite()
    {
        return $this->website;
    }

    public function setPoster($poster)
    {
        $this->poster = $poster;
        return $this;
    }

    public function getPoster()
    {
        return $this->poster;
    }

    public function getPosterTitle()
    {
        return $this->posterTitle;
    }

    public function setPosterTitle($posterTitle)
    {
        $this->posterTitle = $posterTitle;
    }

    public function setCoupon($coupon)
    {
        $this->coupon = $coupon;
        return $this;
    }

    public function getCoupon()
    {
        return $this->coupon;
    }

    public function setRegistrationLink($registrationLink)
    {
        $this->registrationLink = $registrationLink;
        return $this;
    }

    public function getRegistrationLink()
    {
        return $this->registrationLink;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function setDiscount($discount)
    {
        $this->discount = $discount;
        return $this;
    }

    public function getDiscount()
    {
        return $this->discount;
    }

    public function getDiscountType()
    {
        return $this->discountType;
    }

    public function setDiscountType($discountType)
    {
        $this->discountType = $discountType;
    }

    public function getTitleText()
    {
        return $this->titleText;
    }

    public function setTitleText($titleText)
    {
        $this->titleText = $titleText;
    }

    public function getMetaKey()
    {
        return $this->metaKey;
    }

    public function setMetaKey($metaKey)
    {
        $this->metaKey = $metaKey;
    }

    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
    }

    public function getDisplayOrder()
    {
        return $this->displayOrder;
    }

    public function setDisplayOrder($displayOrder)
    {
        $this->displayOrder = $displayOrder;
    }

    public function getRibbonTitle()
    {
        return $this->ribbonTitle;
    }

    public function setRibbonTitle($ribbonTitle)
    {
        $this->ribbonTitle = $ribbonTitle;
    }

    public function getRibbonColor()
    {
        return $this->ribbonColor;
    }

    public function setRibbonColor($ribbonColor)
    {
        $this->ribbonColor = $ribbonColor;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

}

