<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Nelmio\Alice\Fixtures;

class LoadFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        Fixtures::load(__DIR__.'/fixtures.yml',
            $manager, [
                'providers' => [$this]
            ]
        );
    }

    public function professionTitle()
    {
        $professions = [
            'Physician', 'Doctors', 'Nurse', 'Technician', 'Dispenser'
        ];
        $key = array_rand($professions);
        return $professions[$key];
    }

    public function membershipTypeTitle()
    {
        $professions = [
            'Yearly Membership', 'Monthly Membership', 'Weakly Membership'
        ];
        $key = array_rand($professions);
        return $professions[$key];
    }
}