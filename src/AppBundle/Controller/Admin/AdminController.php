<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Service\AdminService;
use AppBundle\Service\EventService;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_ADMIN')")
 */

class AdminController extends Controller
{

    public $adminService;
    public $eventService;

    public function __construct(AdminService $adminService, EventService $eventService)
    {
        $this->adminService  = $adminService;
        $this->eventService  = $eventService;
    }

}