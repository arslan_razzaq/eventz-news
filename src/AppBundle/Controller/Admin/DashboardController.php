<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_ADMIN')")
 */

class DashboardController extends AdminController
{

    /**
    * @Route("/", name="admin_dashboard")
    */
    public function dashboardAction()
    {
        return $this->render('admin/dashboard.html.twig', []);
    }

    


   
}