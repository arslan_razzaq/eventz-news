<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Controller\Admin\AdminController;
use AppBundle\Entity\Event;
use AppBundle\Form\Admin\EventType;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_ADMIN')")
 */
class EventController extends AdminController
{
    /**
     * @Route("/events", name="admin_event")
     */
    public function indexAction()
    {
        $events = $this->getDoctrine()->getManager()->getRepository(Event::class)->findAll();
        return $this->render('event/events.html.twig', [
            'events' => $events
        ]);
    }

    /**
     * @Route("/events/add", name="admin_event_add")
     */
    public function addAction(Request $request)
    {
        $form = $this->createForm(EventType::class);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $this->eventService->saveEvent(new Event(),
                $form->getData(),
                ['uploadPath' => $this->getParameter('conference_poster')], 'add');
            $this->addFlash("success", "Successfully Added.");
            return $this->redirectToRoute('admin_event_add');
        }
        return $this->render('event/event-form.html.twig', [
            'formHeading' => 'Add Event',
            'eventForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/events/{id}/edit", name="admin_event_edit")
     */
    public function editAction(Request $request, Event $event)
    {
        $poster = [
            'fileName' => $event->getPoster(),
            'uploadPath' => $this->getParameter('conference_poster')
        ];
        $form = $this->createForm(EventType::class, $event);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $this->eventService->saveEvent($event, $form->getData(), $poster, 'edit');
            $this->addFlash("success", "Successfully Updated.");
            return $this->redirectToRoute('admin_event_edit', [
                'id' => $event->getId()
            ]);
        }
        return $this->render('event/event-form.html.twig', [
            'formHeading' => 'Edit Event',
            'posterImage' => $event->getPoster(),
            'eventForm' => $form->createView(),
            'selectedCity' => $form->getData()->getCity()->getId()
        ]);
    }

    /**
     * @Route("/events/{id}/delete", name="admin_event_delete")
     */
    public function deleteAction(Event $event)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($event);
        $em->flush();
        $this->addFlash("success", "Successfully Deleted.");
        return $this->redirectToRoute('admin_event');
    }
}
