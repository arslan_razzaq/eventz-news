<?php

namespace AppBundle\Controller\Admin\Settings;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Controller\Admin\AdminController;
use AppBundle\Entity\Category;
use AppBundle\Form\CategoryType;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_ADMIN')")
 */
class CategoryController extends AdminController
{
    /**
     * @Route("/category", name="admin_category")
     */
    public function categoryAction()
    {
        $categories = $this->getDoctrine()->getManager()->getRepository(Category::class)->findAll();
        return $this->render('admin/category/categories.html.twig', [
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/category/add", name="admin_category_add")
     */
    public function categoryAddAction(Request $request)
    {
        $form = $this->createForm(CategoryType::class);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $this->adminService->addCategory($form->getData()->getTitle(), $form->getData()->getOrderBy());
            $this->addFlash("success", "Successfully Added.");
            return $this->redirect($this->generateUrl('admin_category_add'));
        }
        return $this->render('admin/category/category-form.html.twig', [
            'formHeading' => 'Add Category',
            'categoryForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/category/{id}/edit", name="admin_category_edit")
     */
    public function categoryEditAction(Request $request, Category $category)
    {
        if (!$category) {
            throw $this->createNotFoundException('Invalid Category.');
        }
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $this->adminService->updateCategory($category, $form->getData()->getTitle(), $form->getData()->getOrderBy());
            $this->addFlash("success", "Successfully Updated.");
            return $this->redirect($this->generateUrl('admin_category_edit', [
                'id' => $category->getId()
            ]));
        }
        return $this->render('admin/category/category-form.html.twig', [
            'formHeading' => 'Edit Category',
            'categoryForm' => $form->createView()
        ]);
    }
}
