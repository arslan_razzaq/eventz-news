<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Controller\Admin\AdminController;
use AppBundle\Entity\Page;
use AppBundle\Form\PageType;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_ADMIN')")
 */
class PageController extends AdminController
{
    /**
     * @Route("/page", name="admin_page")
     */
    public function indexAction()
    {
        $pages = $this->getDoctrine()->getManager()->getRepository(Page::class)->findAll();
        return $this->render('admin/page/pages.html.twig', [
            'pages' => $pages
        ]);
    }

    /**
     * @Route("/page/add", name="admin_page_add")
     */
    public function addAction(Request $request)
    {
        $form = $this->createForm(PageType::class);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $this->adminService->savePage(new Page(), $form->getData());
            $this->addFlash("success", "Successfully Added.");
            return $this->redirectToRoute('admin_page_add');
        }
        return $this->render('admin/page/page-form.html.twig', [
            'formHeading' => 'Add Page',
            'pageForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/page/{id}/edit", name="admin_page_edit")
     */
    public function editAction(Request $request, Page $page)
    {
        $form = $this->createForm(PageType::class, $page);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $this->adminService->savePage($page, $form->getData());
            $this->addFlash("success", "Successfully Updated.");
            return $this->redirectToRoute('admin_page_edit', [
                'id' => $page->getId()
            ]);
        }
        return $this->render('admin/page/page-form.html.twig', [
            'formHeading' => 'Edit Conference',
            'pageForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/page/{id}/delete", name="admin_page_delete")
     */
    public function deleteAction(Request $request, Page $page)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($page);
        $em->flush();
        $this->addFlash("success", "Successfully Deleted.");
        return $this->redirectToRoute('admin_page');
    }
}
