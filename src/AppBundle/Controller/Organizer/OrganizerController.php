<?php

namespace AppBundle\Controller\Organizer;

use AppBundle\Entity\Organizer;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use AppBundle\Service\OrganizerService;
use AppBundle\Service\EventService;

/**
 * @Route("/organizer")
 * @Security("is_granted('ROLE_ORGANIZER')")
 */

class OrganizerController extends Controller
{

    public $organizerService;
    public $eventService;

    public function __construct(OrganizerService $organizerService, EventService $eventService)
    {
        $this->organizerService = $organizerService;
        $this->eventService     = $eventService;
    }

    /**
     * @Route("/", name="organizer_dashboard")
     */
    public function dashboardAction()
    {
        //return $this->render('organizer/dashboard.html.twig', []);
        return $this->redirectToRoute('organizer_event');
    }
}