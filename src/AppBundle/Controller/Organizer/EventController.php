<?php

namespace AppBundle\Controller\Organizer;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Controller\Organizer\OrganizerController;
use AppBundle\Entity\Event;
use AppBundle\Form\Organizer\EventType;



/**
 * @Route("/organizer")
 * @Security("is_granted('ROLE_ORGANIZER')")
 */
class EventController extends OrganizerController
{
    /**
     * @Route("/events", name="organizer_event")
     */
    public function indexAction()
    {
        $events = $this->getDoctrine()->getManager()
                            ->getRepository(Event::class)
                            ->findBy([
                                'user' => $this->getUser()->getId()
                            ]);
        return $this->render('event/events.html.twig', [
            'events' => $events
        ]);
    }

    /**
     * @Route("/events/add", name="organizer_event_add")
     */
    public function addAction(Request $request)
    {
        $form = $this->createForm(EventType::class);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $this->eventService->saveEvent(new Event(),
                $form->getData(),
                ['uploadPath' => $this->getParameter('conference_poster')], 'add');
            $this->addFlash("success", "Successfully Added.");
            return $this->redirectToRoute('organizer_event_add');
        }
        return $this->render('event/event-form.html.twig', [
            'formHeading' => 'Add Event',
            'eventForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/events/{id}/edit", name="organizer_event_edit")
     */
    public function editAction(Request $request, Event $event)
    {
        $poster = [
            'fileName' => $event->getPoster(),
            'uploadPath' => $this->getParameter('conference_poster')
        ];
        $form = $this->createForm(EventType::class, $event);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $this->eventService->saveEvent($event, $form->getData(), $poster, 'edit');
            $this->addFlash("success", "Successfully Updated.");
            return $this->redirectToRoute('organizer_event_edit', [
                'id' => $event->getId()
            ]);
        }
        return $this->render('event/event-form.html.twig', [
            'formHeading' => 'Edit Event',
            'posterImage' => $event->getPoster(),
            'eventForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/events/{id}/delete", name="organizer_event_delete")
     */
    public function deleteAction(Event $event)
    {
        $isDeleted = $this->eventService->deleteEvent($event);
        $this->addFlash("success", "Successfully Deleted.");
        return $this->redirectToRoute('organizer_event');
    }
}
