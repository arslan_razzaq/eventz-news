<?php

namespace AppBundle\Controller\Front;

use AppBundle\Entity\Category;
use AppBundle\Entity\City;
use AppBundle\Entity\Event;
use AppBundle\Entity\Page;
use AppBundle\Form\ContactFormType;
use AppBundle\Form\PromoteMyEventFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class FrontController extends Controller
{
    public $eventListingLimit = 9;

    /**
     * @Route("/", name="index_show")
     */
    public function indexAction()
    {
        $Categories = $this->getDoctrine()->getRepository(Category::class)->findAll();
        //dump(count($Categories[1]->getEvents()));
        //die();
        return $this->render('front/index.html.twig', ['categories' => $Categories]);
    }

    /**
     * @Route("/events", name="events")
     * @Route("/events/search", name="search_event")
     */
    public function categoryEventsAction(Request $request)
    {
        $country = $request->query->get('country');
        $cities = $request->query->get('cities');
        $months = $request->query->get('months');
        $currentPage = $request->query->get('page', 1);
        $category = $request->query->get('category');

        $repository = $this->getDoctrine()->getRepository(Event::class);
        $events = $repository->getEvents(
            $country, $cities, $months, $currentPage,
            $this->eventListingLimit, $category
        );

        return $this->render('front/listing.html.twig', [
            'selectedCategory' => $category,
            'selectedCountry' => $country,
            'selectedCities' => $cities,
            'selectedMonths' => $months,
            'events' => $events,
            'totalPages' => ceil($events->count() / $this->eventListingLimit),
            'currentPage' => $currentPage
        ]);
    }

    /**
     * @Route("/conference/{slug}")
     * @Route("/events/{slug}", name="event_detail")
     */
    public function eventDetailAction(Event $event)
    {
        return $this->render('front/event-detail.html.twig', [
            'event' => $event
        ]);
    }

    /**
     * @Route("/page/{slug}", name="page_detail")
     */
    public function pageDetailAction($slug)
    {
        $page = $this->getDoctrine()
            ->getManager()
            ->getRepository(Page::class)->findOneBySlug($slug);
        return $this->render('front/page.html.twig', [
            'page' => $page
        ]);
    }

    /**
     * @Route("/contact-us", name="contact_us")
     */
    public function contactUsAction(Request $request, \Swift_Mailer $mailer)
    {
        $form = $this->createForm(ContactFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();
            $message = (new \Swift_Message('Events Discount Contact Form.'))
                ->setFrom('contact@eventsdiscount.com')
                ->setTo('mail@eventsdiscount.com')
                ->setBody(
                    $this->renderView(
                        'emails/contact-form.html.twig',
                        [
                            'senderName' => $formData['yourName'],
                            'senderEmail' => $formData['yourEmail'],
                            'senderSubject' => $formData['yourSubject'],
                            'senderMessage' => $formData['yourMessage']
                        ]
                    ),
                    'text/html'
                )
            ;
            $mailer->send($message);
            $this->addFlash("success", "Successfully Sent.");
            return $this->redirectToRoute('contact_us');
        }

        return $this->render('front/contact.html.twig', [
            'contactForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/promote-my-event", name="promote_my_event")
     */
    public function promoteMyEventAction(Request $request, \Swift_Mailer $mailer)
    {
        $form = $this->createForm(PromoteMyEventFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();
            $message = (new \Swift_Message('Promote My Event Form.'))
                ->setFrom('contact@eventsdiscount.com')
                ->setTo('mail@eventsdiscount.com')
                ->setBody(
                    $this->renderView(
                        'emails/promote-my-event-form.html.twig',
                        [
                            'senderName' => $formData['yourName'],
                            'senderEmail' => $formData['yourEmail'],
                            'senderMobile' => $formData['yourMobile']
                        ]
                    ),
                    'text/html'
                )
            ;
            $mailer->send($message);
            $this->addFlash("success", "Successfully Sent.");
            return $this->redirectToRoute('promote_my_event');
        }

        return $this->render('front/promote-my-twig.html.twig', [
            'promoteMyEventForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/api/country/{id}/cities", name="api_get_cities")
     */
    public function getCountryCitiesAction($id, SerializerInterface $serializer)
    {
        $cities = json_encode([]);
        if($id >= 0){
            $isCities = $this->getDoctrine()->getManager()->getRepository(City::class)->findByCountry($id);
            if($isCities){
                $cities = $serializer->serialize($isCities, 'json');
            }
        }
        return new Response(
            $cities,
            Response::HTTP_OK,
            array('content-type' => 'application/json')
        );
    }

    /*
     * @Route("/search", name="search_conference")
     */
    /*
    public function searchConferenceAction(Request $request)
    {

        $country = $request->query->get('country');
        $cities = $request->query->get('cities');
        $months = $request->query->get('months');
        $currentPage = $request->query->get('page', 1);

        $repository = $this->getDoctrine()->getRepository(Conference::class);
        $conferences = $repository->searchConferences(
            $country, $cities, $months, $currentPage,
            $this->conferenceListingLimit
        );

        return $this->render('front/index.html.twig', [
            'selectedCountry' => $country,
            'selectedCities' => $cities,
            'selectedMonths' => $months,
            'conferences' => $conferences,
            'totalPages' => ceil($conferences->count() / $this->conferenceListingLimit),
            'currentPage' => $currentPage
        ]);
    }
    */
}
