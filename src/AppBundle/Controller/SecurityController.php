<?php
namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\ForgetPasswordToken;
use AppBundle\Entity\Organizer;
use AppBundle\Form\LoginForm;
use AppBundle\Form\MemberChangePasswordForm;
use AppBundle\Form\RegisterForm;
use AppBundle\Service\OrganizerService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\ExecutionContextInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class SecurityController extends Controller
{

    private $organizerService;
    private $authUtils;

    public function __construct(OrganizerService $organizerService, AuthenticationUtils $authUtils)
    {
        $this->organizerService = $organizerService;
        $this->authUtils = $authUtils;
    }
    
    /**
     * @Route("/login", name="security_login")
     */
    public function loginAction(Request $request)
    {
        $loginForm = $this->getLoginForm($this->authUtils);
        return $this->render('security/login.html.twig', array(
            'form'  => $loginForm->form->createView(),
            'error' => $loginForm->error,
        ));
    }

    /**
     * @Route("/logout", name="security_logout")
     */
    public function logoutAction()
    {
        throw new \Exception('this should not be reached!');
    }

    /**
     * @Route("/register", name="security_register")
     */
    public function registerAction(Request $request)
    {
        $isLoggedIn = $this->isLoggedIn();
        if($isLoggedIn !== FALSE){
            return $isLoggedIn;
        }

        $form = $this->createForm(RegisterForm::class);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $user = $this->organizerService->organizerRegister($form->getData());
            if($user != FALSE){
                $this->get('security.authentication.guard_handler')
                    ->authenticateUserAndHandleSuccess(
                        $user,
                        $request,
                        $this->get('app.security.login_form_authenticator'),
                        'main'
                    );
                $isSent = $this->organizerService->sendThankyouEmail($form->getData()->getUsername(), $form->getData()->getName(), $form->getData()->getEmail());
                return $this->redirectToRoute('organizer_event_add');
            }
            else{
                $this->addFlash("error", "Error: Registration Failed");
                return $this->redirectToRoute('security_register');
            }
        }
        return $this->render('security/register.html.twig', [
            'registerForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/forget_password", name="security_forget_password")
     *
     */
    public function forgetPasswordAction(Request $request, \Swift_Mailer $mailer)
    {
        $form = $this->createFormBuilder([])
            ->add('yourEmail', EmailType::class, [
                'label'       => 'Your Email',
                'constraints' => [new NotBlank(), new Email()]
            ])
            ->getForm();

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $formData = $form->getData();
            $token = $this->generateForgetLinkToken($formData['yourEmail']);
            $this->sendForgetLinkByEmail($formData['yourEmail'], $token, $mailer);
            $this->addFlash("success", "Link Send to Your Email '".$formData['yourEmail']."'.");
            return $this->redirect($this->generateUrl('security_forget_password'));
        }

        return $this->render('security/forgetPassword.html.twig', ['forgetForm' => $form->createView()]);
    }

    /**
     * @Route("/reset_password/{token}", name="security_reset_password")
     *
     */
    public function resetPasswordAction(Request $request, $token)
    {
        $em = $this->getDoctrine()->getManager();
        $tokenDetails = $em->getRepository('AppBundle:ForgetPasswordToken')->findOneByToken($token);

        if($tokenDetails != null && $tokenDetails->getStatus() == false){
            $form = $this->createForm(MemberChangePasswordForm::class);
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $user = $em->getRepository('AppBundle:User')->find($tokenDetails->getUser());
                $user->setPlainPassword($form->getData()->getPassword());
                $tokenDetails->setStatus(true);
                $em->flush();
                $this->addFlash("success", "Password Successfully Updated.");
                return $this->redirectToRoute('organizer_dashboard');
            }
            return $this->render('security/resetPassword.html.twig', array(
                'resetPasswordForm' => $form->createView(),
            ));
        }
        else{
            $message = ($tokenDetails != null && $tokenDetails->getStatus() == true)? 'Token Expired.' : 'Token not Valid.';
            throw new \Exception($message);
        }

    }

    /**
     * @Route("/change_password", name="security_change_password")
     */
    public function changePasswordAction(Request $request)
    {
        if($this->get('security.authorization_checker')->isGranted('ROLE_ORGANIZER') || $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
            $form = $this->createForm(MemberChangePasswordForm::class);
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $em = $this->getDoctrine()->getManager();
                $user = $em->getRepository('AppBundle:User')->find($this->getUser()->getId());
                $user->setPlainPassword($form->getData()->getPassword());
                $em->persist($user);
                $em->flush();
                $this->addFlash("success", "Successfully Updated.");
                return $this->redirectToRoute('security_change_password');
            }
            return $this->render('security/changePassword.html.twig', array(
                'changePasswordForm' => $form->createView(),
            ));
        }
        else{
            throw $this->createAccessDeniedException('No Access!');
        }
    }

    private function isLoggedIn()
    {
        $user = $this->getUser();
        if($user != null){
            if(in_array('ROLE_ADMIN', $user->getRoles())){
                return 'admin_dashboard';
            }
            if(in_array('ROLE_MEMBER', $user->getRoles())){
                return 'organizer_dashboard';
            }
        }
        return FALSE;
    }

    private function getLoginForm(AuthenticationUtils $authUtils)
    {
        $isLoggedIn = $this->isLoggedIn();
        if($isLoggedIn !== FALSE){
            return $this->redirectToRoute($isLoggedIn);
        }

        $error = $authUtils->getLastAuthenticationError();
        $lastUsername = $authUtils->getLastUsername();

        $form = $this->createForm(LoginForm::class, [
            '_username' => $lastUsername
        ]);

        return (object) [
            'error' => $error,
            'form' => $form
        ];
    }

    private function generateForgetLinkToken($email)
    {
        $em = $this->getDoctrine()->getManager();

        $organizer = $em->getRepository(Organizer::class)
            ->findOneByEmail($email);

        $token = md5(uniqid(strtotime('now'), true));
        $forgetPasswordToken  = new ForgetPasswordToken;
        $forgetPasswordToken->setUser($organizer->getUser());
        $forgetPasswordToken->setToken($token);
        $em->persist($forgetPasswordToken);
        $em->flush();

        return $token;
    }

    private function sendForgetLinkByEmail($email, $token, $mailer)
    {
        $message = (new \Swift_Message('Events Discount Forget Password.'))
            ->setFrom('no-reply@eventsdiscount.com')
            ->setTo($email)
            ->setBody(
                $this->renderView('emails/forget-form.html.twig',[
                    'token' => $token
                ]),
                'text/html'
            )
        ;
        $mailer->send($message);
    }
    
}