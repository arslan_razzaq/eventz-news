<?php

namespace AppBundle\Validator;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Doctrine\ORM\EntityManager;

class RegisterFormValidator
{
    /**
     * @Assert\NotBlank(message = "Fill the Username")
     */
    private $username;

    /**
     * @Assert\NotBlank(message = "Fill the Password")
     */
    private $password;

    /**
     * @Assert\NotBlank(message = "Fill the name")
     */
    private $name;

    /**
     * @Assert\NotBlank(message = "Fill the Email")
     * @Assert\Email(message = "Email is not Valid")
     */
    private $email;

    /**
     * @Assert\NotBlank(message = "Fill the mobile")
     */
    private $mobile;


    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        $isUsername = $this->em->getRepository('AppBundle:User')
            ->findOneBy(['username' => $this->getUsername()]);

        $isEmail = $this->em->getRepository('AppBundle:Organizer')
            ->findOneBy(['email' => $this->getEmail()]);

        $isMobile = $this->em->getRepository('AppBundle:Organizer')
            ->findOneBy(['mobile' => $this->getMobile()]);

        if($isUsername){
            $context->buildViolation('Username already Taken!')
                ->atPath('username')
                ->addViolation();
        }

        if($isEmail){
            $context->buildViolation('Email already Taken!')
                ->atPath('email')
                ->addViolation();
        }

        if($isMobile){
            $context->buildViolation('Mobile already Taken!')
                ->atPath('mobile')
                ->addViolation();
        }
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getMobile()
    {
        return $this->mobile;
    }

    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }
    
}