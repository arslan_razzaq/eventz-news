<?php

namespace AppBundle\Validator;

use Symfony\Component\Validator\Constraints as Assert;

class MemberChangePasswordValidator
{

    /**
     * @Assert\NotBlank(message = "Fill the Password")
     */
    private $password;

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

}