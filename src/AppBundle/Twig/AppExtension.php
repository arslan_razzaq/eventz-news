<?php

namespace AppBundle\Twig;

use AppBundle\Entity\Category;
use AppBundle\Entity\Country;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;


class AppExtension extends \Twig_Extension
{

    private $em;
    private $requestStack;

    public function __construct(EntityManagerInterface $entityManager,
                                RequestStack $requestStack)
    {
        $this->em = $entityManager;
        $this->requestStack = $requestStack;
    }

    public function getFilters()
    {
        return [];
    }

    public function getFunctions() {
        return [
            new \Twig_SimpleFunction('countries', [$this, 'getCountriesFunction']),
            new \Twig_SimpleFunction('months', [$this, 'getMonthsFunction']),
            new \Twig_SimpleFunction('categories', [$this, 'getCategoriesFunction']),
            new \Twig_SimpleFunction('event_price', [$this, 'eventPriceFunction', ['event']]),
            new \Twig_SimpleFunction('social_share_buttons', [$this, 'getSocialShareButtonsFunction', ['title']]),
        ];
    }

    public function eventPriceFunction($event)
    {
        $priceHtml = '';
        $lineThrough = '';
        if($event->getDiscountType() != null){
            if($event->getDiscountType() == 'a'){
                $price = $event->getPrice();
                $discount = $event->getDiscount();;
                $priceHtml .= '<span class="discount-price">'.($price - $discount).' SAR</span>';
                $lineThrough = 'line-through';
            }
            if($event->getDiscountType() == 'p'){
                $price = $event->getPrice();
                $discount = $event->getDiscount();
                $priceHtml .= '<span class="discount-price">'.($price - (($price * $discount) / 100)).' SAR</span>';
                $lineThrough = 'line-through';
            }
        }
        $priceHtml .= ' <span class="original-price '.$lineThrough.'">'.$event->getPrice().' SAR</span>';
        return $priceHtml;
    }

    public function getCountriesFunction()
    {
        return $this->em->getRepository(Country::class)->findAll();
    }

    public function getMonthsFunction()
    {
        $months = [];
        $this_month = mktime(0, 0, 0, date('m'), 1, date('Y'));
        for ($i = 0; $i < 12; ++$i) {
            $months[] = [
                'value' => date('Y-m', strtotime($i.' month', $this_month)),
                'title' => date('F Y', strtotime($i.' month', $this_month))
            ];
        }
        return $months;
    }

    public function getCategoriesFunction()
    {
        return $this->em->getRepository(Category::class)->findAll();
    }

    public function getSocialShareButtonsFunction($title = '')
    {
        $uri = $this->requestStack->getCurrentRequest()->getUri();
        $baseUrl = $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost();
        $html = '<ul class="social-share-buttons">
            <li>
                <a target="_blank" href="https://www.facebook.com/sharer.php?u='.$uri.'">
                    <img src="'.$baseUrl.'/assets/images/icons/facebook.png">
                </a>
            </li>
            <li>
                <a target="_blank" href="https://twitter.com/intent/tweet?url='.$uri.'&text='.$title.'">
                    <img src="'.$baseUrl.'/assets/images/icons/twitter.png">
                </a>
            </li>
            <li>
                <a target="_blank" href="https://plus.google.com/share?url='.$uri.'">
                    <img src="'.$baseUrl.'/assets/images/icons/googleplus.png">
                </a>
            </li>
            <li>
                <a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url='.$uri.'&title='.$title.'">
                    <img src="'.$baseUrl.'/assets/images/icons/linkedin.png">
                </a>
            </li>
        </ul>';
        return $html;
    }

}